/* 
middleware are the function which has req, res and next
to trigger another middleware we have call next()

middleware is divided into two parts
normal middleware
    (req,res,next)=>{}
    to trigger next middleware we have to call next()
error middleware
    (err,req,res,next)=>{}
    to trigger next error middleware we have to call next("a")


    multer
    it is package which takes images from postman and add images to the public folder
*/
