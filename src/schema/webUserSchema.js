import { Schema } from "mongoose";

let webuserSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required."],
    // lowercase: true,
    // uppercase: true,
    // trim: true,
    // minLength: 2,
    // maxLength: 20,
    validate: (value) => {
      let isValid = /^[a-zA-Z0-9]{2,20}$/.test(value);

      if (!isValid) {
        throw new Error(
          "name should not contain other than alphabet and number and must be at least 2 character and at most 20 character long."
        );
      }
    },
  },
  age: {
    type: Number,
    // required: [true, "age field is required."],
    min: 18,
    max: 70,

    validate: (value) => {
      if (value === 30) {
        throw new Error("age can not be 30");
      }
    },
  },

  phoneNumber: {
    type: Number,
    required: [true, "phoneNumber field is required."],
    // min: 1000000000,
    // max: 9999999999,
    validate: (value) => {
      let strValue = String(value);
      let strLen = strValue.length;
      if (strLen !== 10) {
        let error = new Error("phoneNUmber  must be exact 10 character long");
        throw error;
      }
    },
  },
  password: {
    type: String,
    required: [true, "password field is required."],
    
  },

  roll: {
    type: Number,
    required: [true, "roll field is required."],
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required."],
  },
  spouseName: {
    type: String,
    required: [true, "spouseName field is required."],
  },
  email: {
    type: String,
    required: [true, "email field is required."],
    validate: (value) => {
      let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
        value
      );
      if (!isValid) {
        throw new Error("email not valid.");
      }
    },
  },
  gender: {
    type: String,
    required: [true, "gender field is required."],
    default: "male",
    // validate: (value) => {
    //   if (value === "male" || value === "female" || value === "other") {
    //   } else {
    //     throw new Error("gender can not ber other than male, female , other");
    //   }
    // },
    enum: {
      values: ["male", "female", "other"],
      message: (notEnum) => {
        return `${notEnum.value} is not valid enum`;
      },
    },
  },
  dob: {
    type: Date,
    required: [true, "dob field is required."],
  },
  location: {
    country: {
      type: String,
      required: [true, "country field is required."],
    },
    exactLocation: {
      type: String,
      required: [true, "exactLocation field is required."],
    },
  },

  favTeacher: [
    {
      type: String,
      required: [true, "favTeacher field is required."],
    },
  ],

  favSubject: [
    {
      bookName: {
        type: String,
        required: [true, "bookName field is required."],
      },
      bookAuthor: {
        type: String,
        required: [true, "bookAuthor field is required."],
      },
    },
  ],
});

export default webuserSchema;

// user => name , email, isMarried
/* 
 schema
 model
 router
 index 
*/

/* 
schema design
manipulation
  uppercase
  lowercase
  trim
  default value
validation



*/
