import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import userSchema from "./userSchema.js";
import webuserSchema from "./webUserSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let User = model("User", userSchema);
export let Webuser = model("Webuser", webuserSchema)
export let Product = model("Product", productSchema)
export let Review = model("Review", reviewSchema)

//model singular
// first letter capital
//match
