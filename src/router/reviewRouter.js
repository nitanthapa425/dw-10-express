import { Router } from "express";
import {
  createReview,
  deleteReview,
  readReview,
  readReviewDetails,
  updateReview,
} from "../controller/reviewController.js";

let reviewRouter = Router();

reviewRouter.route("/").post(createReview).get(readReview);

reviewRouter
  .route("/:reviewId")
  .delete(deleteReview)
  .get(readReviewDetails)
  .patch(updateReview);

export default reviewRouter;
