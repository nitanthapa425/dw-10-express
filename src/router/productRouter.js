import { Router } from "express";
import {
  createProduct,
  deleteProduct,
  readProduct,
  readProductDetails,
  updateProduct,
} from "../controller/productController.js";

let productRouter = Router();

productRouter.route("/").post(createProduct).get(readProduct);

productRouter
  .route("/:productId") //localhstudentIdost:8000/student/:
  .delete(deleteProduct)
  .get(readProductDetails)
  .patch(updateProduct);

export default productRouter;
