import { Router } from "express";
import upload from "../middleware/upload.js";
import { handlefile } from "../controller/fileController.js";

let fileRouter = Router();

fileRouter.route("/").post(upload.array("document", 4), handlefile);

export default fileRouter;

//  postman         upload("document")  => req.files

// create api  localhost:8000/images methode post , pass images in field file
// pass  files
// the api must produce the links for that files
