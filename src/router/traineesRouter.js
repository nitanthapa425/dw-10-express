import { Router } from "express";

let traineeRouter = Router();

traineeRouter
  .route("/") //localhost:8000/trainees
  .post((req, res, next) => {
    res.json({
      success: true,
      message: "trainees created successfully.",
    });
  })
  .get((req, res, next) => {
    res.json({
      success: true,
      message: "trainees read successfully",
    });
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "trainees updated successfully",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "trainees deleted successfully",
    });
  });

export default traineeRouter;
