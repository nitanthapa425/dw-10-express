import { Router } from "express";
import { Student } from "../schema/model.js";

let studentRouter = Router();

studentRouter
  .route("/") //localhost:8000/students
  .post(async (req, res) => {
    let studentData = req.body;

    try {
      let result = await Student.create(studentData);
      res.json({
        success: true,
        message: "student created successfully.",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await Student.find({});

      res.json({
        success: true,
        message: "student read successfully.",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

studentRouter
  .route("/:studentId") //localhstudentIdost:8000/student/:
  .delete(async (req, res) => {
    let studentId = req.params.studentId;

    try {
      let result = await Student.findByIdAndDelete(studentId);

      res.json({
        success: true,
        message: "student deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    let studentId = req.params.studentId;
    try {
      let result = await Student.findById(studentId);
      res.json({
        success: true,
        message: "student read successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    let studentId = req.params.studentId;
    let studentData = req.body;

    try {
      let result = await Student.findByIdAndUpdate(studentId, studentData);
      res.json({
        success: true,
        message: "student updated successfully.",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default studentRouter;

// Student.create(studentData)
// Student.find({})
// Student.findById(id)
// Student.findByIdAndDelete(id)
// Student.findByIdAndUpdated(id,data)

// update
// read details


