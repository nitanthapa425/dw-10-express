import { Router } from "express";
import {
  createWebuser,
  deleteWebuser,
  readWebuser,
  readWebuserDetails,
  updateWebuser,
} from "../controller/webuserController.js";
// import { createWebuser, readWebuser } from "../controller/webuserController.js";

let webuserRouter = Router();

webuserRouter.route("/").post(createWebuser).get(readWebuser);

webuserRouter
  .route("/:webuserId") //localhstudentIdost:8000/student/:
  .delete(deleteWebuser)
  .get(readWebuserDetails)
  .patch(updateWebuser);

export default webuserRouter;
