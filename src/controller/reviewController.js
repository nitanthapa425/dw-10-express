import { Review } from "../schema/model.js";

export let createReview = async (req, res) => {
  let reviewData = req.body;

  try {
    let result = await Review.create(reviewData);
    res.json({
      success: true,
      message: "review created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readReview = async (req, res) => {
  try {
    // let result = await Review.find({});
    // let result = await Review.find({ name: "ram" });
    // let result = await Review.find(query);

    let result = await Review.find({})
      .populate("productId", "name price -_id")
      .populate("userId", "name email -_id");

    res.json({
      success: true,
      message: "review read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readReviewDetails = async (req, res) => {
  let reviewId = req.params.reviewId;
  try {
    let result = await Review.findById(reviewId);
    res.json({
      success: true,
      message: "review read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateReview = async (req, res) => {
  let reviewId = req.params.reviewId;
  let reviewData = req.body;

  try {
    let result = await Review.findByIdAndUpdate(reviewId, reviewData);
    res.json({
      success: true,
      message: "review updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteReview = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findByIdAndDelete(reviewId);

    res.json({
      success: true,
      message: "review deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

/* 

1+1 =2
"1"+"1"="11"
"1"+1 ="11"


1-1 =0
"1"-1  => 1-1 =0

*/
