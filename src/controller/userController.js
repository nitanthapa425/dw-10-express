import { User } from "../schema/model.js";
import bcrypt from "bcrypt";
import { sendEmail } from "../utils/sendemail.js";
import { secretKey } from "../constan.js";
import jwt from "jsonwebtoken";

export let createUser = async (req, res) => {
  let userData = req.body;
  let password = userData.password;
  try {
    let hashPassword = await bcrypt.hash(password, 10);
    userData.password = hashPassword;

    let result = await User.create(userData);

    await sendEmail({
      from: "Un <uniqueKc425@gmail.com>",
      to: [req.body.email],
      subject: "Email verification.",
      html: `<h1>You have successfully register.</h1>`,
    });

    res.status(201).json({
      success: true,
      message: "user created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(409).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUser = async (req, res) => {
  let query = req.query; // {name="ram",age="29"}
  let brake = query.brake; //"2"
  let page = query.page; //"3"
  try {
    // let result = await User.find({});
    // let result = await User.find({ name: "ram" });
    // let result = await User.find(query);

    let result = await User.find({})
      .limit(brake) //"2"
      .skip((page - 1) * 2);

    res.status(200).json({
      success: true,
      message: "user read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUserDetails = async (req, res) => {
  let userId = req.params.userId;
  try {
    let result = await User.findById(userId);
    res.status(200).json({
      success: true,
      message: "user read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateUser = async (req, res) => {
  let userId = req.params.userId;
  let userData = req.body;

  try {
    let result = await User.findByIdAndUpdate(userId, userData);
    res.status(201).json({
      success: true,
      message: "user updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUser = async (req, res) => {
  let userId = req.params.userId;

  try {
    let result = await User.findByIdAndDelete(userId);

    res.json({
      success: true,
      message: "user deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUser = async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  console.log(req.body);

  try {
    let user = await User.findOne({ email: email });
    console.log(user);
    let hashPassword = user.password;
    if (user === null) {
      res.json({
        success: false,
        message: "Email or password does not match",
      });
    } else {
      let isValidUser = await bcrypt.compare(password, hashPassword);
      if (isValidUser) {
        //generate token
        let infoObj = {
          id: user._id,
        };
        let expiryInfo = {
          expiresIn: "365d",
        };

        let token = jwt.sign(infoObj, secretKey, expiryInfo);

        res.json({
          success: true,
          message: "User logged in successfully",
          result: token,
        });
      } else {
        res.json({
          success: false,
          message: "Email or password does not match",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let myProfile = async (req, res) => {
  let id = req.id;

  try {
    let result = await User.findById(id);
    res.json({
      success: true,
      message: "Profile read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateMyProfile = async (req, res) => {
  let id = req.id;
  let data = req.body;

  try {
    let result = await User.findByIdAndUpdate(id, data);
    res.json({
      success: true,
      message: "Profile updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteMyProfile = async (req, res) => {
  let id = req.id;

  try {
    let result = await User.findByIdAndDelete(id);
    res.json({
      success: true,
      message: "Profile deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

/* 
register


*/

/* 

post email, password  =
bak get email, password=
find the user whose email is (postman email)=
if not   => success:false, message: "Email or Password does not match"
if user exist
      check password match (using bcrypt)
      if password does not math   => success:false, message: "Email or Password does not match"
      if password match => success:true, message:User logged in successfully.






*/

/* 

1+1 =2
"1"+"1"="11"
"1"+1 ="11"


1-1 =0
"1"-1  => 1-1 =0

*/

// api
// curd (database)
//hashing
//jwt
//convert file to link
//save file to server
//send email

// register
//login
//myprofie
