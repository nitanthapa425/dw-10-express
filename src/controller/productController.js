
import { Product } from "../schema/model.js";

export let createProduct = async (req, res) => {
  let productData = req.body;

  try {
    let result = await Product.create(productData);
    res.json({
      success: true,
      message: "product created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readProduct = async (req, res) => {
  
  try {
    // let result = await Product.find({});
    // let result = await Product.find({ name: "ram" });
    // let result = await Product.find(query);

    let result = await Product.find({})
      

    res.json({
      success: true,
      message: "product read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readProductDetails = async (req, res) => {
  let productId = req.params.productId;
  try {
    let result = await Product.findById(productId);
    res.json({
      success: true,
      message: "product read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateProduct = async (req, res) => {
  let productId = req.params.productId;
  let productData = req.body;

  try {
    let result = await Product.findByIdAndUpdate(productId, productData);
    res.json({
      success: true,
      message: "product updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteProduct = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findByIdAndDelete(productId);

    res.json({
      success: true,
      message: "product deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

/* 

1+1 =2
"1"+"1"="11"
"1"+1 ="11"


1-1 =0
"1"-1  => 1-1 =0

*/
