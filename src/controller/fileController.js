import { serverLink } from "../constan.js";

export let handlefile = (req, res) => {
  let links = req.files.map((value, i) => {
    return `${serverLink}/${value.filename}`;
  });

  console.log(req.body);

  res.json({
    success: true,
    message: "File uploaded successfully",
    result: links,
  });
};
