import { Webuser } from "../schema/model.js";

export let createWebuser = async (req, res) => {
  let webuserData = req.body;

  try {
    let result = await Webuser.create(webuserData);
    res.json({
      success: true,
      message: "webuser created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuser = async (req, res) => {
  try {
    // let result = await Webuser.find({});
    //searching

    // sorting (unlike js mongodb sorting works for number)
    // let result = await Webuser.find({}).sort("name"); // ascending sort
    // let result = await Webuser.find({}).sort("-name");// descending sort
    // let result = await Webuser.find({}).sort("name age");
    // let result = await Webuser.find({}).sort("name -age");

    // select
    //     find has control over the object
    // where as select as control over the object property

    // let result = await Webuser.find({}).select("name age -_id");
    // let result = await Webuser.find({}).select("name age -_id");

    res.json({
      success: true,
      message: "webuser read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;
  try {
    let result = await Webuser.findById(webuserId);
    res.json({
      success: true,
      message: "webuser read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateWebuser = async (req, res) => {
  let webuserId = req.params.webuserId;
  let webuserData = req.body;

  try {
    let result = await Webuser.findByIdAndUpdate(webuserId, webuserData);
    res.json({
      success: true,
      message: "webuser updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteWebuser = async (req, res) => {
  let webuserId = req.params.webuserId;

  try {
    let result = await Webuser.findByIdAndDelete(webuserId);

    res.json({
      success: true,
      message: "webuser deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
