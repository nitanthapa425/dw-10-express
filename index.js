// make an express application
// attach port to that application

import express, { json } from "express";
import connectToMongoDB from "./src/connectToDb/connectToMongoDb.js";
import bikeRouter from "./src/router/bikeRouter.js";
import firstRouter from "./src/router/firstRouter.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import studentRouter from "./src/router/studentRouter.js";
import traineeRouter from "./src/router/traineesRouter.js";
import userRouter from "./src/router/usersRouter.js";
import webuserRouter from "./src/router/webuserRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import { port } from "./src/constan.js";
import cors from "cors";

let expressApp = express();
expressApp.use(json());

expressApp.use(cors());
expressApp.use(express.static("./public"));

expressApp.listen(port, () => {
  console.log(`express application is listening at port ${port}`);
});

connectToMongoDB();

expressApp.use("/firsts", firstRouter);
expressApp.use("/bikes", bikeRouter);
expressApp.use("/trainees", traineeRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/users", userRouter);
expressApp.use("/webusers", webuserRouter);
expressApp.use("/products", productRouter);

expressApp.use("/reviews", reviewRouter);
expressApp.use("/files", fileRouter);

// generate hashPassword
// let password = "nitan123";
// let hashPassword = await bcrypt.hash(password, 10);
// console.log(hashPassword);

// let hashPassword =
//   "$2b$10$Xl6h8eQBC.4RWy8cbOdma.DHt88SWRuoAK2O4vuPuWOt7jv8EK3H2";

// let password = "abc";

// let isValidPassword = await bcrypt.compare(password, hashPassword);
// //connect our application  to mongodb via mongoose
// console.log(isValidPassword);

//localhost:8000/users/login
// methode post
// data :{
//   email:".."
//   password;"..."
// }

//

// let infoObj = {
//   id: "1234214234",
// };

// let secretKey = "dw10";
// let expiryInfo = {
//   expiresIn: "365d",
// };

// let token = jwt.sign(infoObj, secretKey, expiryInfo);

// console.log(token);

// let token =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoibml0YW4iLCJhZ2UiOjI5LCJpYXQiOjE3MDIwMjE2MDAsImV4cCI6MTczMzU1NzYwMH0.cM5CUReV3mJVgWvUlI5iZ9Ne24YYbbXFf5H_nCBhgio";

// try {
//   let infoObj = jwt.verify(token, "dw10");
//   console.log(infoObj);
// } catch (error) {
//   console.log(error.message);
// }

//to be verifed
// token must be made from the given secretkey
// must not expire

//if token is valid => infoObj
//if token is not  valid => error

// [
//   ,
//   "http://localhost:8000/nitan.jpg",
//   "http://localhost:8000/ram.jpg",
// ];

// .env
// statuscode

// register

//login
//my-profile
//update-profile
//update-password

// .env
// always make .env file in root folder
//all variable in .env are string
// in .env we define  special variable (credential(email, password, secretkey) , port , links (database, http://localhost:8000))
// variable name must be declare in upper case
