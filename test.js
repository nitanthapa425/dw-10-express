// [
//     {name:"nitan",age:29, isMarried:false},
//     {name:"sandip",age:25, isMarried:false},

//     {name:"nitan",age:26, isMarried:true},
//     {name:"rishav",age:20, isMarried:false},

//     {name:"nitan",age:29, isMarried:true},
//     {name:"chhimi",age:15, isMarried:true},

//     {name:"narendra",age:27, isMarried:false}=
//     {name:"shidhant",age:16, isMarried:false},

//     {name:"kriston",age:22, isMarried:false},
// ]

brake = 2;
page = (3).skip((page - 1) * brake).limit(brake);

// .find({})
// find({name:"nitan"})
// find({name:"nitan",age:29})
// find({age:27})
// find({age:"27"})
// find({age:22, isMarried:"false"})
// in searching type does not matter
// find({age:25})
//find({age:{$gt:25}})
//find({age:{$gte:25}})
//find({age:{$lt:25}})
//find({age:{$lte:25}})
//find({name:"nitan"})
//find ({name:{$ne:"nitan"}})

//find({age:{$gt:18,$lt:25}})

// find({ name: { $in: ["niki", "anjali", "bidambana"] } });

// task 1
// find age between 15 to 17
// find({age:{$get:15, $lte:17}})

// finding those whose name is nitan , ram , hari
// find({name:{$in:["nitan","ram","hari"]}})
// find({name:{$nin:["nitan","ram","hari"]}})

// $or
// $and

// find({$or:[{name:"nitan"}, {name:"kriston"}]})
// find({$or:[{name:"nitan", age:29}, {name:"kriston"}]})

// find({$or:[{name:"nitan"}, {age:29}]})
// find({$and:[{name:"nitan"}, {age:29}]})

// find those User does not contain name nitan ram hari

/* 
  //for sorting
  [
      {name:"ab",age:60, isMarried:false},
      {name:"ab",age:50, isMarried:false},
      {name:"c",age:40, isMarried:false},
      {name:"b",age:40, isMarried:false},
    {name:"ac",age:29, isMarried:false},
  
]
//output
.find({}).sort("name")
.find({}).sort("-name")
.find({}).sort("name age")
.find({}).sort("name -age")
.find({}).sort("-age -name")
.find({age:40}).sort("name")


[
  {name:"b",age:40, isMarried:false},
  {name:"c",age:40, isMarried:false},

]


find has control over the object
where as select as control over the object property
select
[
  {name:"b",age:40, isMarried:false},
  {name:"c",age:40, isMarried:false},

]

find({name:"c"}).select("age")

[
  {age:40}

]


skip limit


//skip and limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},

    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},

    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},

    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},

    {name:"kriston",age:22, isMarried:false},
]

.find({}).skip("6").limit("2")







//pagination 
.find({}).skip("3")
.find({}).limit("5")
.find({}).skip("3").limit("5")
.find({}).limit("5").skip("2")//wrong
.find({}).skip("2").limit("5")

// order
find, sort, select, skip, limit




//output


[
 {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},

    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},

    {name:"nitan",age:29, isMarried:true},
 
]




hashing
              hashPassword
nitan123   => 4566asdfas12341234  =>database
bcrypt => it is use for hashing



token   (jsonwebtoken)
   generate
        detail 
        logo
        expiryInfo
   validate

   





   statuscode
success
2XX
get , delete =>200
post, patch => 201


failure
4XX

404 => not found
409 => conflict 


.env


register
  data
  hash password 
  save
  email

login
    email, password
    if user with that email exist
    if user exist check password match or not
    if match  generate token 
    send token to postman
my-profile
     pass token from postman
     get token in backend
     validate token
     if  not validate  throw error
     if validate

     
  

update-profile
update-password


cors
cross
origin 
resource
sharing



 */
